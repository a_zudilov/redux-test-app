import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { push } from "react-router-redux";
import { removeClient } from "../modules/clients";

export class ClientRowComponent extends React.Component {
    render() {
        const { name, surname, email, id, changePage, removeClient } = this.props;
        return (
            <tr>
                <td>{ name }</td>
                <td>{ surname }</td>
                <td>{ email }</td>
                <td className="text-center">
                    <button
                        id="view"
                        className="btn btn-primary"
                        onClick={ () => changePage( `/client/${id}/display` ) }>
                        Просмотр
                    </button>
                </td>
                <td className="text-center">
                    <button
                        id="remove"
                        className="btn btn-primary"
                        onClick={() => removeClient( id )}>
                        Удалить клиента
                    </button>
                </td>
                <td className="text-center">
                    <button
                        id="comments"
                        className="btn btn-primary"
                        onClick={ () => changePage( `/client/${id}/comments` ) }>
                        Комментарии
                    </button>
                </td>
            </tr>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators( {
    removeClient,
    changePage: ( path ) => push( path )
}, dispatch );


export default connect( null, mapDispatchToProps )( ClientRowComponent );
