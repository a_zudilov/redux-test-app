import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { push } from "react-router-redux";

import { loadClient, updateClient, addClient } from "../modules/clients";
import ModalsComponent from "./modal";
import { openModal } from "../modules/modal";
import { addComment } from "../modules/comments";
import { MESSAGES } from "./placeholdersMessages";


const Form = ( props ) => <div className="needs-validation" noValidate>{ props.children }</div>;
const FormRowEdit = ( props ) =>
    <div className="col-md-6 mb-3">
        <label>{ props.label }</label>
        <input type="text"
            className={ `form-control ${ props.isError ? "invalid" : ""}`}
            value={ props.value }
            onChange={ props.onChange }
            required
            placeholder={ props.placeholder }
            aria-describedby={ `${ props.name }Help` }
        />
        <small
            id={ `${ props.name }Help` }
            className="form-text text-muted">
            { props.describe }
        </small>

    </div>;

export class AddAndEditClientComponent extends React.Component {
    constructor( props ) {
        super( props );
        const { id } = this.props.match.params;
        if ( id ) {
            props.loadClient( id );
        }
        this.stringRegexp = new RegExp( /[a-zA-Zа-яА-ЯёЁ]/g );
        this.numberRegexp = new RegExp( /[0-9]/g );
        this.emailRegexp = new RegExp( /\w*@\w*\.[a-z]*/g );
        this.clearState = {
            name: "",
            surname: "",
            phone: "",
            email: "",
            country: "",
            city: "",
            address: "",
            postcode: "",
            id: null
        };
        const { clients } = this.props;
        const client = clients.find( ( client ) => client.id === +id ) || {};
        this.state = Object.assign( {}, this.clearState, client );
    }

    handleInput( field, event ) {
        const newState = {
            [ field ]: event.target.value,
            [ `${field}Changed` ]: true
        };
        this.setState( newState );
    }

    openModal() {
        this.props.openModal( {
            id: "add-client-modal",
            type: "confirmation",
            text: "Вы уверены, что хотите сохранить изменения?",
            onClose: () => console.log( "fire at closing event" ),
            onConfirm: () => this.editData()
        } );
    }


    editData() {
        const { name, surname, phone, email, country, city, address, postcode, id } = this.state;
        let invalid = false;
        const isEdit = this.props.match.url.indexOf( "edit" ) !== -1;
        const validState = {
            nameError: false,
            surnameError: false,
            phoneError: false,
            emailError: false,
            countryError: false,
            cityError: false,
            addressError: false,
            postcodeError: false
        };

        const stringParams = [
            { key: "name", value: name },
            { key: "surname", value: surname },
            { key: "country", value: country },
            { key: "city", value: city }
        ];
        stringParams.map( ( param ) => {
            if ( param.value.replace( this.stringRegexp, "" ).length || !param.value.length ) {
                validState[ `${param.key}Error` ] = true;
                invalid = true;
            }
            return true;
        } );

        const numberParams = [
            { key: "phone", value: phone },
            { key: "postcode", value: postcode }
        ];
        numberParams.map( ( param ) => {
            if ( param.value.replace( this.numberRegexp, "" ).length || !param.value.length ) {
                validState[ `${param.key}Error` ] = true;
                invalid = true;
            }
            return true;
        } );

        if ( email.replace( this.emailRegexp, "" ).length || !email.length ) {
            validState.emailError = true;
            invalid = true;
        }

        if ( invalid ) {
            this.setState( validState );
        } else {
            this.setState( this.clearState );
            if ( isEdit ) {
                this.props.updateClient( id,
                    { name, surname, phone, email, country, city, address, postcode, id } );
            } else {
                this.props.addClient(
                    { name, surname, phone, email, country, city, address, postcode } );
            }
            this.props.changePage( isEdit ? `/client/${id}/display` : "/clients" );
        }
    }

    addComment( props ) {
        const { id } = this.state;
        const author = "Система";
        const date = Date.now();
        const type = "simple";
        const comment = props;
        this.props.addComment( { id, comment: { author, comment, type, date } } );
    }

    componentWillReceiveProps( nextProps ) {
        if ( JSON.stringify( this.state ) === JSON.stringify( this.clearState ) ) {
            const { clients } = nextProps;
            const id = +nextProps.match.params.id;
            const client = clients.find( ( client ) => client.id === id ) || {};
            this.setState( client );
        }
    }

    render() {
        const { id } = this.state;
        const isEdit = this.props.match.url.indexOf( "edit" ) !== -1;
        const {
            name, surname, phone, email, country, city, address, postcode, nameError,
            surnameError, phoneError, emailError, countryError, cityError, addressError,
            postcodeError
        } = this.state;

        const rows = [
            [ {
                name: "name", label: "Имя", isError: nameError, value: name,
                placeholder: MESSAGES.name, describe: MESSAGES.nameDescribe
            },
            {
                name: "country", label: "Страна", isError: countryError, value: country,
                placeholder: MESSAGES.country, describe: MESSAGES.countryDescribe
            } ],
            [ {
                name: "surname", label: "Фамилия", isError: surnameError, value: surname,
                placeholder: MESSAGES.surname, describe: MESSAGES.surnameDescribe
            },
            {
                name: "city", label: "Город", isError: cityError, value: city,
                placeholder: MESSAGES.city, describe: MESSAGES.cityDescribe
            } ],
            [ {
                name: "phone", label: "Телефон", isError: phoneError, value: phone,
                placeholder: MESSAGES.phone, describe: MESSAGES.phoneDescribe
            },
            {
                name: "address", label: "Адрес", isError: addressError, value: address,
                placeholder: MESSAGES.address, describe: MESSAGES.addressDescribe
            } ],
            [ {
                name: "email", label: "Email", isError: emailError, value: email,
                placeholder: MESSAGES.email, describe: MESSAGES.emailDescribe
            },
            {
                name: "postcode", label: "Индекс", isError: postcodeError, value: postcode,
                placeholder: MESSAGES.postcode, describe: MESSAGES.postcodeDescribe
            } ]
        ];
        return (
            <div className="input-form">
                <Form>
                    {
                        rows.map( ( row, i ) =>
                            <div className="form-row" key={`form-row-${i}`}>
                                {
                                    row.map( (
                                        { name, value, isError, label, placeholder, describe },
                                        j ) =>
                                        <FormRowEdit
                                            onChange={this.handleInput.bind( this, name )}
                                            value={value}
                                            label={label}
                                            isError={isError}
                                            name={name}
                                            key={`edit-field-${j}`}
                                            placeholder={placeholder}
                                            describe={describe}
                                        />
                                    )
                                }
                            </div>

                        )
                    }
                    <button
                        className="btn btn-primary"
                        onClick={ this.openModal.bind( this ) }>
                        {isEdit ? "Сохранить" : "Зарегистрировать"}
                    </button>
                    <button
                        className="btn btn-secondary"
                        onClick={() => this.props.changePage(
                            isEdit ? `/client/${id}/display` : "/clients" )}>
                        Отмена
                    </button>
                </Form>
                <ModalsComponent/>
            </div>
        );
    }
}


function mapStateToProps( state ) {
    return { clients: [ ...state.clients.clients ] };
}

const mapDispatchToProps = dispatch => bindActionCreators( {
    updateClient,
    loadClient,
    changePage: ( path ) => push( path ),
    openModal,
    addComment,
    addClient
}, dispatch );


export default connect( mapStateToProps, mapDispatchToProps )( AddAndEditClientComponent );
