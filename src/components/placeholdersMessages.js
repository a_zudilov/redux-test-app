export const MESSAGES = {
    name: "Введите имя клиента",
    surname: "Введите фамилию клиента",
    country: "Введите страну клиента",
    city: "Введите город клиента",
    address: "Введите адрес клиента",
    phone: "Введите телефон клиента",
    email: "Введите email клиента",
    postcode: "Введите индекс клиента",
    phoneDescribe: "Например: 1234567890",
    emailDescribe: "Например: example@example.ex",
    postcodeDescribe: "Например: 123456",
    nameDescribe: "Например: Иван",
    surnameDescribe: "Например: Петров",
    cityDescribe: "Например: Екатеринбург",
    countryDescribe: "Например: Россия",
    addressDescribe: "Например: улица Ленина, дом 54, квартира 17"
};