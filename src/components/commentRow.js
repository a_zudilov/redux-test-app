import React from "react";
import moment from "moment";

const CommentRowComponent = ( { author, comment, type, date } ) =>
    <tr>
        <td>{ moment( date ).utcOffset( 7 * 60 ).format( "DD.MM.YYYY, HH:mm:ss" ) }</td>
        <td>{ author }</td>
        <td>{ comment }</td>
        <td>{ type }</td>
    </tr>;

export default CommentRowComponent;