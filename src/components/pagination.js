import React from "react";
import { NavLink } from "react-router-dom";

function pageActive( match, location, index ) {
    if ( index === 1 && !location.search ) {
        return true;
    }
    return location.search === `?page=${index}`;
}

export default class PaginationComponent extends React.Component {


    render() {
        const pages = new Array( this.props.pageCount );
        const { pathName } = this.props;
        pages.fill( true );
        return (
            <div className="pagination">
                { pages.map( ( page, index ) =>
                    <NavLink
                        className="page-item"
                        activeClassName="active"
                        isActive={( match, location ) => pageActive( match, location, index + 1 )}
                        key={`pagination-page-${index + 1}`}
                        to={{
                            pathname: pathName,
                            search: `?page=${index + 1}`
                        }}>
                        <span className="page-link">
                            {index + 1}
                        </span>
                    </NavLink>
                ) }
            </div>
        );
    }
}
