import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import ClientRowComponent from "./clientRow";
import { loadClients } from "../modules/clients";
import PaginationComponent from "./pagination";

const PER_PAGE = 5;

export class ClientListComponent extends React.Component {
    constructor( props ) {
        super( props );
        const { location } = this.props;
        let currentPage = +location.search.replace( "?page=", "" );
        if ( !currentPage ) {
            currentPage = 1;
        }
        props.loadClients( { offset: currentPage, limit: PER_PAGE } );
        const { clients } = this.props;
        this.state = { clients, currentPage };
    }

    componentWillReceiveProps( nextProps ) {
        const { loadClients, location } = nextProps;
        const { currentPage } = this.state;
        const page = +location.search.replace( "?page=", "" ) || 1;
        if ( page !== currentPage ) {
            this.setState( { ...this.state, currentPage: page } );
            loadClients( { offset: page, limit: PER_PAGE } );
        }
        if ( nextProps.clients.length > PER_PAGE ||
            nextProps.clients.length === 0 ){
            loadClients( { offset: currentPage, limit: PER_PAGE } );
        }
    }

    render() {
        const { clients, pageCount } = this.props;
        const pathName = "/clients";
        return (
            <div className="mt-3 offset-2 col-8">
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col" className="text-center">Имя</th>
                            <th scope="col" className="text-center">Фамилия</th>
                            <th scope="col" className="text-center">Email</th>
                            <th scope="col" colSpan={3} className="text-center">Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        {clients.map( ( client, index ) => <ClientRowComponent {...client}
                            key={`client_row_${index}`}/> )}
                    </tbody>
                </table>
                <PaginationComponent pageCount={ pageCount } pathName={ pathName }/>
            </div>
        );
    }
}

function mapStateToProps( state ) {
    return {
        clients: [ ...state.clients.clients ],
        pageCount: state.clients.pageCount
    };
}

const mapDispatchToProps = dispatch => bindActionCreators( { loadClients }, dispatch );


export default connect( mapStateToProps, mapDispatchToProps )( ClientListComponent );
