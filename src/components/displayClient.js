import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { push } from "react-router-redux";

import { loadClient } from "../modules/clients";


const Form = ( props ) => <div className="form">{ props.children }</div>;
const FormRowDisplay = ( props ) =>
    <div className="col-md-6 mb-3">
        <label >{ props.label }</label>
        <input
            className="form-control"
            type="text"
            value={ props.value }
            readOnly/>
    </div>;
export class DisplayClientComponent extends React.Component {
    constructor( props ) {
        super( props );
        const { id } = this.props.match.params;
        props.loadClient( id );
        const { clients } = this.props;
        const client = clients.find( ( client ) => client.id === +id ) || {};
        this.state = Object.assign( {}, client );
    }

    componentWillReceiveProps( nextProps ) {
        const { clients } = nextProps;
        const id = +nextProps.match.params.id;
        const client = clients.find( ( client ) => client.id === id ) || {};
        this.setState( client );
    }

    render() {
        const { id, name, surname, phone, email, country, city, address, postcode } = this.state;
        const rows = [
            [ { name: "name", label: "Имя", value: name },
                { name: "country", label: "Страна", value: country } ],
            [ { name: "surname", label: "Фамилия", value: surname },
                { name: "city", label: "Город", value: city } ],
            [ { name: "phone", label: "Телефон", value: phone },
                { name: "address", label: "Адрес", value: address } ],
            [ { name: "email", label: "Email", value: email },
                { name: "postcode", label: "Индекс", value: postcode } ]
        ];
        return (
            <div className="input-form">
                <Form>
                    {
                        rows.map( ( row, i ) =>
                            <div className="form-row" key={`form-row-${i}`}>
                                {
                                    row.map( ( { value, label }, j ) =>
                                        <FormRowDisplay
                                            key={`display-field-${j}`}
                                            value={value}
                                            label={label}/>
                                    )
                                }
                            </div>
                        )
                    }
                    <div>
                        <button className="button-edit-link"
                            onClick={() =>
                                this.props.changePage( `/client/${id}/edit` )}>
                            Редактировать
                        </button>
                    </div>
                </Form>
            </div>
        );
    }
}


function mapStateToProps( state ) {
    return { clients: [ ...state.clients.clients ] };
}

const mapDispatchToProps = dispatch => bindActionCreators( {
    loadClient,
    changePage: ( path ) => push( path )
}, dispatch );


export default connect( mapStateToProps, mapDispatchToProps )( DisplayClientComponent );