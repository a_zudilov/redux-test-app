import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { push } from "react-router-redux";

import { addComment, loadComments } from "../modules/comments";
import CommentRowComponent from "../components/commentRow";
import ModalsComponent from "./modal";
import { openModal, closeModal } from "../modules/modal";
import PaginationComponent from "./pagination";

const PER_PAGE = 10;

class CustomModalContent extends React.Component {
    constructor( props ) {
        super( props );
        this.state = this.props.state;
    }

    authorInput( event ) {
        this.setState( { author: event.target.value } );
    }

    commentInput( event ) {
        this.setState( { comment: event.target.value } );
    }

    typeInput( event ) {
        this.setState( { type: event.target.value } );
    }

    render() {
        let author, comment;
        const radioButtons = [
            {
                key: "Обычный",
                value: "simple"
            },
            {
                key: "Инфо",
                value: "info"
            },
            {
                key: "Важный",
                value: "important"
            }
        ];
        return (
            <React.Fragment>
                <div className="modal-body">
                    <div className="input-group">
                        <label className="col-3 mt-3" htmlFor="author">Автор</label>
                        <textarea
                            className="col-9 mt-3 form-control mx-auto"
                            rows="1"
                            resize="none"
                            name="author"
                            value={ author }
                            onChange={ this.authorInput.bind( this ) }
                        />
                    </div>
                    <div className="input-group">
                        <label className="col-3 mt-3" htmlFor="comment">Комментарий</label>
                        <textarea rows="4"
                            className="col-9 mt-3 form-control mx-auto"
                            name="comment"
                            resize="none"
                            value={ comment }
                            onChange={ this.commentInput.bind( this ) }>
                        </textarea>
                    </div>
                    <div className="mx-auto col-10">
                        <label className="mx-auto">Тип</label>
                        {
                            radioButtons.map(
                                ( x, i ) =>
                                    <p key={`radio-${i}`}>
                                        <div className="custom-control custom-radio">
                                            <input
                                                type="radio"
                                                name="type"
                                                value={ x.value }
                                                className="custom-control-input"
                                                id={`customRadio${i}`}
                                                onChange={ this.typeInput.bind( this ) }/>
                                            <label
                                                className="custom-control-label"
                                                htmlFor={`customRadio${i}`}
                                            >
                                                { x.key }
                                            </label>
                                        </div>
                                    </p>
                            )
                        }
                    </div>
                </div>
                <div className="modal-footer">
                    <button
                        className="btn btn-primary"
                        onClick={ () => this.props.onConfirm( this.state ) }>
                        Подтвердить
                    </button>
                    <button
                        className="btn btn-secondary"
                        onClick={ () => this.props.onClose() }>Отменить
                    </button>
                </div>
            </React.Fragment>
        );
    }
}

class CommentsListComponent extends React.Component {
    constructor( props ) {
        super( props );
        const { id } = this.props.match.params;
        const { location } = this.props;
        let currentPage = +location.search.replace( "?page=", "" );
        if ( !currentPage ) {
            currentPage = 1;
        }
        props.loadComments( { id, offset: currentPage, limit: PER_PAGE } );
        const { comments } = this.props;
        this.state = { comments, currentPage };
    }

    updateState() {
        const { comments } = this.props;
        const { id } = this.props.match.params;
        const current = comments.find( ( x ) => x.id === +id ) || {};
        this.setState( current );
    }

    saveData( state ) {
        const { author, comment, type } = state;
        this.setState( state );
        const date = Date.now();
        const { id } = this.props.match.params;
        this.props.addComment( { id, comment: { author, comment, type, date } } );
        this.props.closeModal( { id: "add-comment-modal" } );
    }

    openModal() {
        const { id } = this.state;
        if ( !id ) {
            this.updateState();
        }
        this.props.openModal( {
            id: "add-comment-modal",
            type: "custom",
            onClose: () => console.log( "fire at closing event" ),
            onConfirm: () => this.editData(),
            content: <CustomModalContent {...this}
                onClose={ () => this.props.closeModal( { id: "add-comment-modal" } ) }
                onConfirm={ this.saveData.bind( this ) }/>
        } );
    }

    componentWillReceiveProps( nextProps ) {
        const { loadComments, location } = nextProps;
        const { currentPage } = this.state;
        const { id } = this.props.match.params;
        if ( nextProps.comments.length > PER_PAGE ){
            loadComments( { id, offset: currentPage, limit: PER_PAGE } );
        }
        const page = +location.search.replace( "?page=", "" ) || 1;
        if ( page !== currentPage ) {
            this.setState( { currentPage: page } );
            loadComments( { id, offset: page, limit: PER_PAGE } );
        }
    }

    render() {
        const { comments, pageCount, loadComments } = this.props;
        const { id } = this.props.match.params;
        const pathName = `/client/${id}/comments`;
        if ( !comments ) {
            loadComments( { id, offset: 1, limit: PER_PAGE } );
            return <div></div>;
        }
        return (
            <div className="mt-3 offset-2 col-8">
                <ModalsComponent/>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col" className="text-center">Дата</th>
                            <th scope="col" className="text-center">Автор</th>
                            <th scope="col" className="text-center">Комментарий</th>
                            <th scope="col" className="text-center">Тип</th>
                        </tr>
                    </thead>
                    <tbody>
                        { comments.map( ( object, index ) => <CommentRowComponent { ...object }
                            key={`comment_row_${index}`}/>
                        ) }
                    </tbody>
                </table>
                <div>
                    <button
                        className="btn btn-primary float-right"
                        onClick={ this.openModal.bind( this ) }
                    >
                        Добавить комментарий
                    </button>
                    <PaginationComponent pageCount={ pageCount } pathName={ pathName }/>
                </div>
            </div>
        );
    }
}

function mapStateToProps( state ) {
    return {
        comments: [ ...state.comments.comments ],
        pageCount: state.comments.pageCount
    };
}

const mapDispatchToProps = dispatch => bindActionCreators( {
    loadComments,
    changePage: ( path ) => push( path ),
    openModal,
    closeModal,
    addComment
}, dispatch );


export default connect( mapStateToProps, mapDispatchToProps )( CommentsListComponent );
