export const ADD_CLIENT = "clients/ADD_CLIENT";
export const UPDATE_CLIENT = "clients/UPDATE_CLIENT";
export const REMOVE_CLIENT = "clients/REMOVE_CLIENT";
export const LOAD_CLIENTS = "clients/LOAD_CLIENTS";
export const LOAD_CLIENT = "clients/LOAD_CLIENT";

const initialState = { clients: [], pageCount: 1 };

export default ( state = initialState, action ) => {
    switch ( action.type ) {
    case ADD_CLIENT:
        return addClientHandler( action, state );
    case UPDATE_CLIENT:
        return updateClientHandler( action, state );
    case REMOVE_CLIENT:
        return removeClientHandler( action, state );
    case LOAD_CLIENTS:
        return loadClientsHandler( action, state );
    case LOAD_CLIENT:
        return loadClientHandler( action, state );
    default:
        return state;
    }
};

const addClientHandler = function( action, state ){
    let clients;
    console.log( "add client" );
    if ( action.source === "server" ){
        clients = action.clients;
        return {
            ...state,
            clients
        };
    } else if ( action.source === "localStorage" ) {
        clients = [ ...state.clients ];
        clients.push( { ...action.data, id: Date.now() } );
        saveToLocalStorage( "clientList", clients );
        return {
            ...state,
            clients
        };
    }
    return state;
};
const removeClientHandler = function( action, state ){
    console.log( "remove" );
    if ( action.source === "server" ){
        state.clients = [];
        return { ...state };
    } else if ( action.source === "localStorage" ) {
        const clients = [ ...state.clients ];
        const client = clients.find( x => x.id === action.id );
        const index = clients.indexOf( client );
        if ( index === -1 ) {
            return state;
        }
        clients.splice( index, 1 );
        saveToLocalStorage( "clientList", clients );
        return {
            ...state,
            clients
        };
    }
    return state;
};
const loadClientsHandler = function( action, state ){
    console.log( "load all clients" );
    if ( action.source === "server" ){
        const clients = action.clients;
        const pageCount = action.pageCount;
        state.clients = clients;
        return {
            ...state,
            pageCount
        };
    } else if ( action.source === "localStorage" ){
        return {
            ...state,
            clients: readToLocalStorage( "clientList" )
        };
    }
    return state;
};
const loadClientHandler = function( action, state ){
    console.log( "load one client" );
    const clients = [ ...state.clients ];
    const client = clients.find( x => x.id === action.client.id );
    if ( !client ) {
        clients.push( action.client );
        return {
            ...state,
            clients
        };
    }
    Object.assign( client, action.client );
    return {
        ...state,
        clients: clients
    };
};
const updateClientHandler = function( action, state ) {
    console.log( "update" );
    const clients = [ ...state.clients ];
    const client = clients.find( x => x.id === action.id );
    if ( !client ) {
        return state;
    }
    Object.assign( client, action.data );
    if ( action.source === "localStorage" ) {
        saveToLocalStorage( "clientList", clients );
    }
    return {
        ...state,
        clients
    };
};

function saveToLocalStorage( key, data ) {
    localStorage.setItem( key, JSON.stringify( data ) );
}

function readToLocalStorage( key ) {
    return JSON.parse( localStorage.getItem( key ) ) || [];
}

export const addClient = ( data ) => {
    return dispatch => {
        return fetch( "http://localhost:3001/clients",
            {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify( data )
            } )
            .then( function ( responce ) {
                return responce.json();
            } )
            .then( function ( json ) {
                dispatch( { type: ADD_CLIENT, source: "server", clients: json.clients } );
            } )
            .catch( function () {
                dispatch( { type: ADD_CLIENT, source: "localStorage", data } );
            } );
    };
};

export const updateClient = ( id, data ) => {
    return dispatch => {
        return fetch( `http://localhost:3001/clients/${id}`,
            {
                method: "put",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify( data )
            } )
            .then( function ( responce ) {
                return responce.json();
            } )
            .then( function ( json ) {
                dispatch( { type: UPDATE_CLIENT, source: "server", id, data: json.client } );
            } )
            .catch( function () {
                dispatch( { type: UPDATE_CLIENT, source: "localStorage" }, id, data );
            } );
    };
};

export const removeClient = ( id ) => {
    return dispatch => {
        return fetch( `http://localhost:3001/clients/${id}`, { method: "delete" } )
            .then( function(){
                dispatch( { type: REMOVE_CLIENT, source: "server" } );
            } )
            .catch( function () {
                dispatch( { type: REMOVE_CLIENT, id, source: "localStorage" } );
            } );
    };
};

export const loadClients = ( data ) => {
    return dispatch => {
        return fetch( "http://localhost:3001/clients" +
            `?limit=${ data.limit }&offset=${ data.offset }`,
        { method: "GET" } )
            .then( function ( responce ) {
                return responce.json();
            } )
            .then( function ( json ) {
                dispatch(
                    {
                        type: LOAD_CLIENTS,
                        source: "server",
                        clients: json.clients,
                        pageCount: json.pageCount
                    } );
            } )
            .catch( function () {
                dispatch( { type: LOAD_CLIENTS, source: "localStorage" } );
            } );
    };
};

export const loadClient = ( id ) => {
    return dispatch => {
        return fetch( `http://localhost:3001/clients/${id}`, { method: "GET" } )
            .then( function ( responce ) {
                return responce.json();
            } )
            .then( function ( json ) {
                dispatch( { type: LOAD_CLIENT, source: "server", client: json.client } );
            } )
            .catch( function () {
                dispatch( { type: LOAD_CLIENT, source: "localStorage" } );
            } );
    };
};
