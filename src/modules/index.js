import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import clients from "./clients";
import modals from "./modal";
import comments from "./comments";

export default combineReducers( {
    routing: routerReducer,
    clients,
    modals,
    comments
} );