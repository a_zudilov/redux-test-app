export const ADD_COMMENT = "comments/ADD_COMMENT";
export const LOAD_COMMENTS = "comments/LOAD_COMMENTS";

const initialState = { comments: [], pageCount: 1 };

export default ( state = initialState, action ) => {
    let comments = null, pageCount = 1;
    switch ( action.type ) {
    case ADD_COMMENT:
        console.log( "add comment" );
        comments = action.comments;
        return {
            ...state,
            comments
        };
    case LOAD_COMMENTS:
        console.log( "load comments" );
        comments = action.comments;
        pageCount = action.pageCount;
        state.comments = comments;
        return {
            ...state,
            pageCount
        };
    default:
        return state;
    }
};

export const addComment = ( data ) => {
    return dispatch => {
        return fetch( `http://localhost:3001/comments/${data.id}`,
            {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify( data.comment )
            } )
            .then( function ( responce ) {
                return responce.json();
            } )
            .then( function ( json ) {
                dispatch( { type: ADD_COMMENT, comments: json.comments } );
            } )
            .catch( function ( err ) {
                console.log( err );
            } );
    };
};
export const loadComments = ( data ) => {
    return dispatch => {
        return fetch(
            "http://localhost:3001/comments" +
            `/${ data.id }?limit=${ data.limit }&offset=${ data.offset }`,
            { method: "GET" } )
            .then( function ( responce ) {
                return responce.json();
            } )
            .then( function ( json ) {
                dispatch( {
                    type: LOAD_COMMENTS,
                    comments: json.comments,
                    pageCount: json.pageCount
                } );
            } )
            .catch( function ( err ) {
                console.log( err );
            } );
    };
};