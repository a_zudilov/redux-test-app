import React from "react";
import AddAndEditClientComponent from "../components/addAndEditClient";


export default class ClientAddAndEditPage extends React.Component {
    render() {
        return (
            <div>
                <AddAndEditClientComponent {...this.props}/>
            </div>
        );
    }
}