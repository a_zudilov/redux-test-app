import React from "react";
import ClientListComponent from "../components/clientList";


export default class ClientList extends React.Component {
    render() {
        return (
            <div>
                <ClientListComponent {...this.props}/>
            </div>
        );
    }
}