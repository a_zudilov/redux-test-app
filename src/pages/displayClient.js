import React from "react";
import DisplayClientComponent from "../components/displayClient";


export default class DisplayClientPage extends React.Component {
    render() {
        return (
            <div>
                <DisplayClientComponent {...this.props}/>
            </div>
        );
    }
}