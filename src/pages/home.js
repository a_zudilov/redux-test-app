import React from "react";
import { push } from "react-router-redux";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

const HomePage = () =>
    <div className="mt-3 offset-2 col-8">
        <p>Одностраничное приложение для хранения информации о клиентах</p>
    </div>
;

const mapStateToProps = state => ( { ...state.counter } );


const map = dispatch => bindActionCreators( { changePage: () => push( "/clients" ) }, dispatch );

export default connect( mapStateToProps, map )( HomePage );
