import React from "react";
import CommentsListComponent from "../components/commentsList";

export default class CommentsPage extends React.Component{
    render() {
        return (
            <CommentsListComponent {...this.props}/>
        );
    }
}
