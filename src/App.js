import React from "react";
import { Route, NavLink } from "react-router-dom";
import ClientAddAndEditPage from "./pages/clientAddAndEdit";
import ClientList from "./pages/clients";
import DisplayClientPage from "./pages/displayClient";
import CommentsPage from "./pages/comments";
import HomePage from "./pages/home";
import "./App.css";

const App = () =>
    <div>
        <ul className="nav nav-tabs">
            <li className="nav-item">
                <NavLink className="nav-link"
                    activeClassName="active"
                    to="/clients"
                    exact>
                    Список клиентов
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link"
                    activeClassName="active"
                    to="/clients/add-client"
                    exact>
                    Добавить клиента
                </NavLink>
            </li>
        </ul>
        <main>
            <Route exact path="/" component={ HomePage }/>
            <Route exact path="/clients" component={ ClientList }/>
            <Route exact path="/clients/add-client" component={ ClientAddAndEditPage }/>
            <Route exact path="/client/:id/display" component={ DisplayClientPage }/>
            <Route exact path="/client/:id/edit" component={ ClientAddAndEditPage }/>
            <Route exact path="/client/:id/comments" component={ CommentsPage }/>
        </main>

    </div>
;

export default App;
