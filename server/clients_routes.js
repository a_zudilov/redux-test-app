const express = require( "express" );
const router = express.Router();
const fs = require( "fs" );
const fileName = "bd.json";
const LIMIT_DEFAULT = 5;
const OFFSET_DEFAULT = 1;
const CONSTS = require( "./messageConst" );

router.use( function timeLog( req, res, next ) {
    console.log( "Time: ", Date.now() );
    next();
} );
const readFile = () => JSON.parse( fs.readFileSync( fileName, "utf-8" ) );
const writeFile = data => fs.writeFileSync( fileName, JSON.stringify( data ), "utf-8" );
// Здесь будем обрабатывать запросы
router.route( "/" )
    .get( function( req, res ) {
        const limit = +req.query.limit || LIMIT_DEFAULT;
        const offset = +req.query.offset || OFFSET_DEFAULT;
        const bd = readFile();
        const { clients } = bd;
        const { length } = clients;
        const pageCount = Math.ceil( length / limit );
        if ( length > limit ) {
            const start = ( offset - 1 ) * limit;
            const end = offset * limit;
            const currentPage = clients.slice( start, end );
            const clientsPage = currentPage.map( x => {
                return {
                    name: x.name,
                    surname: x.surname,
                    email: x.email,
                    id: x.id
                };
            } );
            res.json( { clients: clientsPage, pageCount } );
        } else {
            res.json( { clients: clients, pageCount } );
        }
    } )
    .post( function ( req, res ){
        const bd = readFile();
        const { increment, clients, comments } = bd;
        clients.push( { id: increment, ...req.body } );
        comments.push( { id: increment, comments: [] } );
        bd.increment = increment + 1;
        writeFile( bd );
        res.json( { clients } );
    } );
router.route( "/:id" )
    .delete( function( req, res ){
        const bd = readFile();
        let { clients, comments } = bd;
        clients = clients.filter( ( x ) => x.id !== +req.params.id );
        comments = comments.filter( ( x ) => x.id !== +req.params.id );
        bd.clients = clients;
        bd.comments = comments;
        writeFile( bd );
        res.send( "remove success" );
    } )
    .put( function( req, res ) {
        const bd = readFile();
        const clients = bd.clients;
        let client = clients.find( client => client.id === +req.params.id );
        const newClient = req.body;
        const comments = bd.comments;
        const currentObjectComments = comments.find( x => x.id === client.id );
        const oldComments = currentObjectComments ? currentObjectComments.comments : [];
        const newComments = Object.keys( newClient )
            .filter( x => newClient[ x ] !== client[ x ] )
            .map( x => {
                return { key: x, old: client[ x ], new: newClient[ x ] };
            } );
        newComments.map( x => {
            const constName = x.key.toUpperCase() + "_CONST";
            const comment = [
                CONSTS[ constName ],
                x.old,
                CONSTS.ON_CONST,
                x.new
            ].join( " " );
            oldComments.push(
                {
                    author: CONSTS.AUTHOR,
                    comment,
                    type: "simple",
                    date: Date.now()
                }
            );
        } );
        client = Object.assign( client, req.body );
        bd.clients = Object.assign( clients, client );
        writeFile( bd );
        res.json( { client } );
    } )
    .get( function( req, res ){
        const bd = readFile();
        const clients = bd.clients;
        const client = clients.find( client => client.id === +req.params.id );
        res.json( { client } );
    } );

module.exports = router;