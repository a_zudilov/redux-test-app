const express = require( "express" );
const bodyParser = require( "body-parser" );
const clients = require( "./clients_routes" );
const comments = require( "./comments_routes" );
const app = express();


const port = 3001;

app.use( function( req, res, next ) {
    res.header( "Access-Control-Allow-Origin", "*" );
    res.header( "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Z-Key" );
    res.header( "Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS" );
    next();
} );

app.use( bodyParser.json() );
app.use( "/clients", clients );
app.use( "/comments", comments );


app.use( express.static( "../build" ) );

app.listen( port, function () {
    console.log( "Example app listening on port " + port );
} );