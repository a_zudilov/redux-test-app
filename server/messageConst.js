const CONSTS = {
    AUTHOR: "Система",
    NAME_CONST: "Администратор изменил имя с",
    SURNAME_CONST: "Администратор изменил фамилию с",
    EMAIL_CONST: "Администратор изменил email с",
    PHONE_CONST: "Администратор изменил телефон с",
    COUNTRY_CONST: "Администратор изменил страну с",
    CITY_CONST: "Администратор изменил город с",
    ADDRESS_CONST: "Администратор изменил адрес с",
    POSTCODE_CONST: "Администратор изменил индекс с",
    ON_CONST: "на"
};

module.exports = CONSTS;