const express = require( "express" );
const router = express.Router();
const fs = require( "fs" );
const fileName = "bd.json";
const LIMIT_DEFAULT = 5;
const OFFSET_DEFAULT = 1;

router.use( function timeLog( req, res, next ) {
    console.log( "Time: ", Date.now() );
    next();
} );
const readFile = () => JSON.parse( fs.readFileSync( fileName, "utf-8" ) );
const writeFile = data => fs.writeFileSync( fileName, JSON.stringify( data ), "utf-8" );

router.get( "/:id", function( req, res ){
    const limit = +req.query.limit || LIMIT_DEFAULT;
    const offset = +req.query.offset || OFFSET_DEFAULT;
    const bd = readFile();
    const comments = bd.comments;
    const curObjectComments = comments.find( x => x.id === +req.params.id );
    const current = curObjectComments ? curObjectComments.comments : [];
    const { length } = current;
    const pageCount = Math.ceil( length / limit );
    if ( length > limit ) {
        const start = ( offset - 1 ) * limit;
        const end = offset * limit;
        const commentsPage = current.slice( start, end );
        res.json( { comments: commentsPage, pageCount } );
    } else {
        res.json( { comments: current, pageCount } );
    }
} );
router.post( "/:id", function( req, res ){
    const bd = readFile();
    const comments = bd.comments;
    const current = comments.find( x => x.id === +req.params.id );
    current.comments.push( req.body );
    bd.comments = comments;
    writeFile( bd );
    res.json( { comments: current.comments } );
} );

module.exports = router;