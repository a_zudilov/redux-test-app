import clientReducers, { ADD_CLIENT, ADD_COMMENT, LOAD_CLIENTS, REMOVE_CLIENT, UPDATE_CLIENT } from "../../src/modules/clients";

const initialState = {
    "clients": [
        {
            "name": "one",
            "surname": "one",
            "phone": "1111111111",
            "email": "one@one.one",
            "country": "one",
            "city": "one",
            "address": "one",
            "postcode": "111111",
            "id": 1,
            "comments": []
        },
        {
            "name": "two",
            "surname": "two",
            "phone": "2222222222",
            "email": "two@two.two",
            "country": "two",
            "city": "two",
            "address": "two",
            "postcode": "222222",
            "id": 2,
            "comments": [
                {
                    "author": "Система",
                    "comment": "Comment 1",
                    "type": "simple",
                    "date": 1528825371372
                },
                {
                    "author": "Система",
                    "comment": "Comment 2",
                    "type": "simple",
                    "date": 1528825397201
                },
                {
                    "author": "Система",
                    "comment": "Comment 3",
                    "type": "simple",
                    "date": 1528825429336
                }
            ]
        }
    ]
};

describe( "clients reducer test", () => {
    // it( "add new client", () => {
    //     const newClient = {
    //         "name": "three",
    //         "surname": "three",
    //         "phone": "3333333333",
    //         "email": "three@three.three",
    //         "country": "three",
    //         "city": "three",
    //         "address": "three",
    //         "postcode": "333333",
    //         "id": 3
    //     };
    //     const state = clientReducers( initialState, { type: ADD_CLIENT, data: newClient } );
    //     const newLength = state.clients.length;
    //     expect( newLength ).toEqual(3);
    //     expect( state.clients[newLength - 1] ).toEqual( newClient );
    // } );
    it( "update existing client", () => {
        const newData = { "id": 2, "surname": "newSurname", "address": "newAddress"};
        const state = clientReducers( initialState, { type: UPDATE_CLIENT, id: newData.id, data: newData } );
        const length = state.clients.length;
        const editedClient = {...state.clients[length - 1]};
        expect( length ).toEqual(2);
        expect( editedClient )
            .toEqual( Object.assign( initialState.clients[1], newData ) );
    } );
    it( "update not existing client", () => {
        const newData = { "id": 3, "surname": "newSurname", "address": "newAddress"};
        const state = clientReducers( initialState, { type: UPDATE_CLIENT, id: newData.id, data: newData } );
        const stateClients = [...state.clients];
        const length = stateClients.length;
        expect( length ).toEqual(2);
        expect( stateClients ).toEqual( initialState.clients );
    } );
    // it( "remove existing client", () => {
    //     const removedId = 1;
    //     const state = clientReducers( initialState, { type: REMOVE_CLIENT, id: removedId } );
    //     const newClients = [...state.clients];
    //     const newLength = newClients.length;
    //     expect( newLength ).toEqual(1);
    //     expect( newClients ).toEqual([[...initialState.clients].pop()]);
    // } );
    it( "remove not existing client", () => {
        const removedId = 5;
        const state = clientReducers( initialState, { type: REMOVE_CLIENT, id: removedId } );
        const newClients = [...state.clients];
        const newLength = newClients.length;
        expect( newLength ).toEqual(2);
        expect( newClients ).toEqual(initialState.clients);
    } );
    // it( "add comment", () => {
    //     const newComment = {
    //         "author":"Система",
    //         "comment":"newComment",
    //         "type":"simple",
    //         "date":1
    //     };
    //     const editedId = 1;
    //     const state = clientReducers( initialState, { type: ADD_COMMENT, data: { id: editedId, comment: newComment } } );
    //     const editedClient = {...[...state.clients].shift()};
    //     const newCommentLength = editedClient.comments.length;
    //     const addedComment = editedClient.comments[newCommentLength - 1];
    //     expect(newCommentLength).toEqual(1);
    //     expect(addedComment).toEqual(newComment);
    // } );
} );