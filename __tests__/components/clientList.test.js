// import React from "react";
// import { shallow, mount, render } from "enzyme";
// import renderer from "react-test-renderer";
// import ConnectedClientListComponent, { ClientListComponent } from "../../src/components/clientList";
// import configureStore from "redux-mock-store";
// import { Provider } from "react-redux";
//
// const changePage = jest.fn();
// const removeClient = jest.fn();
// const loadClients = jest.fn();
//
// const initialProps = {
//     clients: {
//         clients: [
//             {
//                 name: "nameTest",
//                 surname: "surnameTest",
//                 email: "emailTest",
//                 id: 1,
//             },
//             {
//                 name: "nameTest2",
//                 surname: "surnameTest2",
//                 email: "emailTest2",
//                 id: 2,
//             }
//         ],
//         pageCount: 1
//     },
//     loadClients
// };
//
// // const location = { search: "?page=1" };
//
// const newInitialProps = {
//     clients: {
//         clients: [
//             {
//                 name: "nameTest3",
//                 surname: "surnameTest3",
//                 email: "emailTest3",
//                 id: 3,
//             },
//             {
//                 name: "nameTest4",
//                 surname: "surnameTest4",
//                 email: "emailTest4",
//                 id: 4,
//             }
//         ],
//         pageCount: 1
//     },
//     loadClients
// };
//
// describe( "client list", () => {
//     let wrapper, store;
//     const mockStore = configureStore();
//     beforeEach( () => {
//         store = mockStore( initialProps );
//         wrapper = mount(<Provider store={store}><ConnectedClientListComponent /></Provider>);
//     } );
//     it( "snapshot test", () => {
//         let tree = renderer
//             .create(<Provider store={store}><ConnectedClientListComponent /></Provider>)
//             .toJSON();
//         expect(tree).toMatchSnapshot();
//     } );
//     it( "render to connected component", () => {
//         expect( wrapper.find(ConnectedClientListComponent).length ).toBe(1);
//     } );
//     it( "check props matches connected component", () => {
//         expect( wrapper.find(ClientListComponent).prop( "clients" ) ).toEqual(initialProps.clients.clients);
//     } );
//     const getState = (actions) => {
//         // console.log(actions[0].type);
//         if (actions[0]&& actions[0].type==="new"){
//             return newInitialProps;
//         } else {
//             return initialProps;
//         }
//     };
//     let newWrapper;
//     it( "test new props", () => {
//         store = mockStore( getState );
//         newWrapper = mount(<Provider store={store}><ConnectedClientListComponent /></Provider>);
//         expect( newWrapper.find( "tbody" ).text() ).toEqual( "nameTestsurnameTestemailTestПросмотрУдалить клиентаКомментарииnameTest2surnameTest2emailTest2ПросмотрУдалить клиентаКомментарии" );
//         store.dispatch({"type": "new"});
//         console.log("store: "+JSON.stringify(store.getState()));
//         // wrapper.setState( newInitialProps );
//         expect( newWrapper.find( "tbody" ).text() ).toEqual("nameTest3surnameTest3emailTest3ПросмотрУдалить клиентаКомментарииnameTest4surnameTest4emailTest4ПросмотрУдалить клиентаКомментарии");
//     } );
//
//
// } );
test("",()=>{});