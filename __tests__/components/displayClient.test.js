import React from "react";
import { DisplayClientComponent } from "../../src/components/displayClient.js";
import { render } from "enzyme";
import renderer from "react-test-renderer";

const changePage = jest.fn();
const loadClient = jest.fn();
const initialState = {
    clients: [
        {
            changePage: changePage,
            "name": "one",
            "surname": "one",
            "phone": "1111111111",
            "email": "one@one.one",
            "country": "one",
            "city": "one",
            "address": "one",
            "postcode": "111111",
            "id": 1
        },
        {
            changePage: changePage,
            "name": "two",
            "surname": "two",
            "phone": "2222222222",
            "email": "two@two.two",
            "country": "two",
            "city": "two",
            "address": "two",
            "postcode": "222222",
            "id": 2
        }
    ],
    match: { "params": { "id": 1 } },
    loadClient: loadClient
};

describe( "display client test", () => {
    let wrapper;

    beforeEach( ()=>{
        wrapper = render( <DisplayClientComponent {...initialState}/> );
    } );

    it( "display client correct", () => {
        expect( wrapper.find( "input.form-control" ).length ).toBe( 8 );
    } );
    it( "display button", () => {
        expect( wrapper.find( "button" ).length ).toBe( 1 );
    } );
    it( "snapshot test", () => {
        const tree = renderer
            .create( <DisplayClientComponent {...initialState}/> )
            .toJSON();
        expect( tree ).toMatchSnapshot();
    } );

    let button;

    //todo: не заработал
    // it( "button clicked", () => {
    //     button = wrapper.find( ".button-edit-link" );
    //     button.simulate( "click" );
    //     expect( changePage ).toBeCalled();
    // } );

} );