import React from "react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import { ClientRowComponent } from "../../src/components/clientRow";

const changePage = jest.fn();
const removeClient = jest.fn();

const initialProps = {
    name: "nameTest",
    surname: "surnameTest",
    email: "emailTest",
    id: 1,
    changePage: changePage,
    removeClient: removeClient
};

describe( "client row component test", () => {
    let wrapper;
    beforeEach( () => {
        wrapper = shallow( <ClientRowComponent {...initialProps}/> );
    } );
    it( "display td and button", () => {
        expect( wrapper.find( "td" ).length ).toBe(6);
        expect( wrapper.find( "button" ).length ).toBe(3);
    } );
    let button;
    it( "button view test", () => {
        button = wrapper.find( "#view" );
        button.simulate( "click" );
        expect( changePage ).toBeCalled();
    } );
    it( "button remove test", () => {
        button = wrapper.find( "#remove" );
        button.simulate( "click" );
        expect( removeClient ).toBeCalled();
    } );
    it( "button comments test", () => {
        button = wrapper.find( "#comments" );
        button.simulate( "click" );
        expect( changePage ).toBeCalled();
    } );
    it( "snapshot test: renders correctly", () => {
        const tree = renderer
            .create(<ClientRowComponent {...initialProps}/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
    it( "mock changePage 2 calls", () => {
        expect(changePage.mock.calls.length).toBe(2);
    } );
    it( "mock changePage display params", () => {
        expect(changePage.mock.calls[0][0]).toBe("/client/1/display");
    } );
    it( "mock changePage comments params", () => {
        expect(changePage.mock.calls[1][0]).toBe("/client/1/comments");
    } );
    it( "mock removeClients 1 calls", () => {
        expect(removeClient.mock.calls.length).toBe(initialProps.id);
    } );
    it( "mock removeClient params", () => {
        expect(removeClient.mock.calls[0][0]).toBe(initialProps.id);
    } );
} );