import React from "react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import PaginationComponent from "../../src/components/pagination";
import {MemoryRouter as Router} from "react-router-dom";

const initialProps = {
    pageCount: 4
};

describe( "pagination test", () => {
    let wrapper;
    beforeEach( () => {
        wrapper = shallow( <PaginationComponent {...initialProps}/>);
    } );

    it( "numbers of Link", () => {
        expect( wrapper.find( "NavLink" ).length ).toBe(4);
    } );

    it( "snapshot test", () => {
        const tree = renderer
            .create(<Router><PaginationComponent {...initialProps}/></Router>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    } );

} );