import React from "react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import CommentRowComponent from "../../src/components/commentRow";

const initialProps = {
    author: "testAuthor",
    comment: "testComment",
    type: "simple",
    date: 123456
};

describe( "commentRow tests", () => {
    let wrapper;
    beforeEach( () => {
        wrapper = shallow( <CommentRowComponent {...initialProps}/> );
    } );

    it( "test numbers of td's", () => {
        expect( wrapper.find( "td" ).length ).toBe(4);
    } );
    it( "snapshot test", () => {
        const tree = renderer
            .create(<CommentRowComponent {...initialProps}/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    } );
} );