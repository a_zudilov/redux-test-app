module.exports = {
    setupFiles: [
        "<rootDir>/__tests__/setupTests.js",
    ],
    testMatch: [
        "**/?(*.)+(spec|test).js?(x)"
    ]
};